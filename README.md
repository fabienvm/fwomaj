# Fwomaj

Fromaj est un petit lecteur multimedia qui permet de visionner/éditer ("dérusher") rapidement des fichiers videos pour un montage ultérieur.
Modification du taux images/seconde, découpage du début et de la fin, changement de container, etc.

Fwomaj is a small media player to quickly view and edit video files (it's called "derushing") for later editing.
Change framerate and other encoding parameters, trim the start and end, etc.

![screenshot](https://bitbucket.org/yassinphilip/fwomaj/raw/master/screenshot.jpg)

## Install
`wget https://bitbucket.org/yassinphilip/fwomaj/downloads/fwomaj_0.3_all.deb && sudo dpkg -i fwomaj_0.3_all.deb`

Or build the .deb package yourself:
```
git clone https://yassinphilip@bitbucket.org/yassinphilip/fwomaj.git
cd fwomaj
./build_package.sh
sudo gdebi ../fwomaj_0.3_all.deb
```


Icon by [Buuf](http://mattahan.deviantart.com)

Philip Yassin [WWW](http://yassinphilip.bitbucket.org) | Help me make it [Patreon](https://www.patreon.com/yassinphilip)
