#!/usr/bin/env bash

if debuild -I -us -uc; then
    find ../ \( -name "fwomaj*.deb" -o -name "fwomaj*.tar*" \) -exec curl -v -u philipyassin -X POST https://api.bitbucket.org/2.0/repositories/philipyassin/fwomaj/downloads/ -F files=@{} \;
fi
